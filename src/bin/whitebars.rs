//! Is this a bug in the Rust implementation of Handlebars?

use std::error;

use expect_exit::{ExpectedResult, ExpectedWithError};
use serde::Serialize;

const TEMPLATE_UNQUOTED: &str = "#!/usr/bin/perl

my %alt_ops = (
    {{#each synonyms}}
    {{this.name}} => '{{this.sym}}',
    {{/each}}
);
";

const TEMPLATE_QUOTED: &str = "#!/usr/bin/perl

my %alt_ops = (
    {{#each synonyms}}
    '{{this.name}}' => '{{this.sym}}',
    {{/each}}
);
";

#[derive(Debug, Serialize)]
struct Synonym<'a> {
    name: &'a str,
    sym: &'a str,
}

#[derive(Debug, Serialize)]
struct SimpleTop<'a> {
    synonyms: [Synonym<'a>; 6],
}

const SIMPLE: SimpleTop = SimpleTop {
    synonyms: [
        Synonym {
            name: "lt",
            sym: "<",
        },
        Synonym {
            name: "le",
            sym: "<=",
        },
        Synonym {
            name: "eq",
            sym: "<",
        },
        Synonym {
            name: "ne",
            sym: "!=",
        },
        Synonym {
            name: "ge",
            sym: ">=",
        },
        Synonym {
            name: "gt",
            sym: ">",
        },
    ],
};

fn render_template(template: &str) -> Result<String, Box<dyn error::Error>> {
    println!("About to parse a template: {:?}", template);
    let mut renderer = handlebars::Handlebars::new();
    renderer.set_strict_mode(true);
    renderer.register_escape_fn(|value| value.to_string());
    renderer.register_template_string("perl", template)?;

    println!("About to render that template against {:?}", SIMPLE);
    renderer
        .render("perl", &SIMPLE)
        .expect_result(|| format!("Render {:?} against {:?}", template, SIMPLE))
}

#[cfg(test)]
mod tests {
    #[test]
    fn test_quoted() {
        let res = crate::render_template(crate::TEMPLATE_QUOTED).unwrap();
        println!("{}", res);
        assert!(res.contains("'lt' => '<'"));
    }

    #[test]
    fn test_unquoted() {
        let res = crate::render_template(crate::TEMPLATE_UNQUOTED).unwrap();
        println!("{}", res);
        assert!(res.contains("lt => '<'"));
    }
}

fn main() {
    println!(
        "Quoted:\n{}",
        render_template(TEMPLATE_QUOTED).or_exit_e_("Quoted")
    );
    println!(
        "Unquoted:\n{}",
        render_template(TEMPLATE_UNQUOTED).or_exit_e_("Unquoted")
    );
}
