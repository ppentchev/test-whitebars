The whitebars program attempts to render two templates producing
Perl source code that defines an array of comparison operator synonyms.

However, it seems that if I omit the quotes around the "this.name"
expression, the Rust implementation of Handlebars will remove the space
character that immediately follows it. Is this a bug in that implementation,
or have I missed something in Handlebars's handling of whitespace?

The result of rendering the "quoted" template:

    Quoted:                                                    
    #!/usr/bin/perl                                            
                                                               
    my %alt_ops = (                                            
        'lt' => '<',                                           
        'le' => '<=',                                          
        'eq' => '<',
        'ne' => '!=',
        'ge' => '>=',
        'gt' => '>',
    );

The result of rendering the "unquoted" template:

    Unquoted:
    #!/usr/bin/perl
    
    my %alt_ops = (
        lt=> '<',
        le=> '<=',
        eq=> '<',
        ne=> '!=',
        ge=> '>=',
        gt=> '>',
    );
